<?php

    //Function load trong php
    function sendContactMail( WP_REST_Request $request ) {
        $response = array(
            'status'  => 304,
            'message' => 'There was an error sending the form.'
        );

        $siteName = wp_strip_all_tags( trim( get_option( 'blogname' ) ) );
        $contactEmail = $request['contact_email'];
        $contactPhone = $request['contact_phone'];

        $subject = "[$siteName] This email was sent from landing page";
        $postdata = http_build_query(
            array(
                'email' => $contactEmail,
                'phone' => $contactPhone
            )
        );

        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );

        $context  = stream_context_create($opts);

        $result = file_get_contents('./email-notification-admin.php', false, $context);
        $body = "";

        $to = get_option( 'admin_email' );
        $headers = array(
            'Content-Type: text/html; charset=UTF-8',
            "Reply-To: $contactEmail",
        );

        if ( wp_mail( $to, $subject, $body, $headers ) ) {
            $response['status'] = 200;
            $response['message'] = 'Form sent successfully.';
            $response['test'] = $body;
        }

        return new WP_REST_Response( $response );
    }

    $request = $_REQUEST;
    if(!($request['phone'] && $request['email']))
        return false;
?>
<div style="
    border-radius: 10px;
    width: -webkit-fit-content;
    width: -moz-fit-content;
    width: fit-content;
    max-width: 600px;
    position: relative;
    margin: 80px auto;
    padding: 20px;
    line-height: 30px;
    color: #8a6d3b;
    text-shadow: 0 1px 1px white;
    background-color: #fcf8e3;
    background-image: -webkit-radial-gradient(center, cover, #ffffffb3, #ffffff1a 90%), -webkit-repeating-linear-gradient(top, transparent, transparent 29px, #efcfadb3 29px, #efcfadb3 30px);

    background-image: -moz-radial-gradient(center, cover, #ffffffb3, #ffffff1a 90%), -moz-repeating-linear-gradient(top, transparent, transparent 29px, #efcfadb3 29px, #efcfadb3 30px);

    background-image: -o-radial-gradient(center, cover, #ffffffb3, #ffffff1a 90%), -o-repeating-linear-gradient(top, transparent, transparent 29px, #efcfadb3 29px, #efcfadb3 30px);


    border: 1px solid #faebcc;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;

    -webkit-box-shadow: inset 0 1px #ffffff80, inset 0 0 5px #d8e071, 0 0 1px #0000001a, 0 2px #00000005;
    box-shadow: inset 0 1px #ffffff80, inset 0 0 5px #d8e071, 0 0 1px #0000001a, 0 2px #00000005;
">

    <div style="
        border-radius: 10px;
        padding: 30px;
        line-height: 2;
        color: #8a6d3b;
    ">
        <div style="margin-bottom: 15px;">
            <img src="https://www.balooworld.ca/wp-content/uploads/2018/11/Balooworld-logo-550x244.png" alt="" width="200px" height="auto">
        </div>
        <div style="font-size: 14px; font-weight: 600; text-transform: uppercase; color: #8a6d3b;">This email was sent from <a href="http://balooworld.surge.sh/" target="_blank">Landing page</a>
        </div>
        <div style="
            margin-bottom: 20px;
            font-size: 20px;
            line-height: 1.5;
            color: #8a6d3b;
            font-weight: 600;
            text-transform: uppercase;
        ">Your information is
        </div>
        <div style="font-size: 14px; color: #8a6d3b;">
            <span>Email: <strong><?php echo $request['email']?></strong></span>
        </div>
        <div style="font-size: 14px; color: #8a6d3b;">
            <span>Telephone Number: <strong><?php echo $request['phone']?></strong></span>
        </div>
    </div>
</div>