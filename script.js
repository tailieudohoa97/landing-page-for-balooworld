//Effect WOW
new WOW().init();


$(window).scroll(function () {
    if ($(window).scrollTop() >= 10) {
        $('header#header').addClass('change-menu');
    } else {
        $('header#header').removeClass('change-menu');
    }
});

$(function () {
    'use strict'

    // Open Menu main on Mobile
    $("[data-trigger]").on("click", function(){
        var trigger_id =  $(this).attr('data-trigger');
        $(trigger_id).toggleClass("show");
        $('body').toggleClass("offcanvas-active");
    });

    // close if press ESC button 
    $(document).on('keydown', function(event) {
        if(event.keyCode === 27) {
            $(".navbar-collapse").removeClass("show");
            $("body").removeClass("overlay-active");
        }
    });

    // close button 
    $(".btn-close, #myScrollspy a.nav-link").click(function(e){
        $(".navbar-collapse").removeClass("show");
        $("body").removeClass("offcanvas-active");
    }); 
})

var swiper = new Swiper(".swiper", {
    slidesPerView: 2,   //1 item trên 1 slide (nên mặc định cho màn hình nhỏ nhất)
    spaceBetween: 10,   //Khoảng cách giữa các item (nên mặc định cho màn hình nhỏ nhất)
    slidesPerGroup: 1,  //Trượt 1 cái 1 lần
    loop: true,         //Vòng lặp
    autoplay: {
        delay: 2500,    //Thời gian trượt
        disableOnInteraction: true, //stop khi nắm kéo 1 item, Cho phép dừng khi tương tác giao diện
    },
    loopFillGroupWithBlank: true,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    breakpoints: { //Giới hạn số lượng item trên 1 slide theo responsive
        1025: { //min-width: 1025px
            //slidesPerView: 3, //ver 1
            slidesPerView: 5, //ver 2
            spaceBetween: 30,
        },
        768: { //min-width: 768px
            slidesPerView: 2,
            spaceBetween: 20,
        },
    }
});

//HOVER SẼ DỪNG CHẠY SLIDER
$('.swiper').on('mouseover', function() {
    swiper.autoplay.stop();
});

$('.swiper').on('mouseout', function() {
    swiper.autoplay.start();
});




//TẠO VÒNG LẶP - TỰ ĐỘNG THÊM HÌNH VÀO CÁC ELEMENT ĐƯỢC LẶP LẠI NHIỀU LẦN
// - Phải có tên hình
// - Tạo vòng lặp theo số lượng tên hình được liệt kê
var arr = [
"ghe (1).jpeg",
"ghe (10).jpeg",
"ghe (11).jpeg",
"ghe (12).jpeg",
"ghe (13).jpeg",
"ghe (14).jpeg",
"ghe (15).jpeg",
"ghe (16).jpeg",
"ghe (17).jpeg",
"ghe (18).jpeg",
"ghe (19).jpeg",
"ghe (2).jpeg",
"ghe (21).jpeg",
"ghe (22).jpeg",
"ghe (3).jpeg",
"ghe (4).jpeg",
"ghe (5).jpeg",
"ghe (6).jpeg",
"ghe (7).jpeg",
"ghe (8).jpeg",
"ghe (9).jpeg",
"giuong (1).jpeg",
"giuong (2).jpeg",
"giuong (3).jpeg",
"giuong (4).jpeg",
"goi (1).jpg",
"goi (1).png",
"goi (2).jpg",
"goi (3).jpg",
"goi (4).jpg",
"goi (5).jpg",
"goi (6).jpg",
"goi (7).jpg"];
arr.forEach(function(img,index){
    $('.swiper-wrapper').append('\
        <div class="swiper-slide">\
            <div class="card h-100">\
               <img src="./images/products/'+img+'"/>\
            </div>\
        </div>\
    ')
    // console.log(img);
    // console.log(index); 
});
